import { api } from "./api.js";

import { isletter, addletter,check,back} from "./busines-logic.js";



if(!navigator.onLine){
    alert("Sorry your internet Connection is down!! please try after some time ")
}else{


// variables 

const letters = document.querySelectorAll(".row .sb");

const ldiv = document.querySelector(".box")

const maxlength = 5;

async function game() {

    
    let done = false;


   const word = await api();


   const wordParts = word.split("");

    
    document.addEventListener('keydown', function handlekeypress(event) {

        const action = event.key;
        if (action === "Enter" && done == false) {

        done =   check(action,wordParts,word,done);
           
        } else if (action === 'Backspace' && done == false) {

            back();

        }
        else if (isletter(action) && action.length == 1 && done == false) {

            addletter(action.toUpperCase());
        }
    });

}

game();

}