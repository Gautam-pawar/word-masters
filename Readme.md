# Word Masters game website 

## Hosting link : https://gautam-word-masters.netlify.app/

The webpage is buit and designed as a Word guessing game.

## Game is built using 

* HTML
* css
* JavaScript
* API : (https://words.dev-apis.com/word-of-the-day) 

### The website is part of assignment during the course by Frontend Masters.

### How to play the game

* player gets six chances to guess the correct word of the day.

* during this six attempts, player get hints for evert correct letter location by green color code  around the letter.

* player also gets hint for every wrong letter which does not exist in the word by Red clolor code.

* player also gets hint for existing letter by yello color code.

* If the player guesses the right word before or upto the six attempt the player  wins else the player looses . 

