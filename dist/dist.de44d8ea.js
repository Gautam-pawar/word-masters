// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"dist/index.js":[function(require,module,exports) {
var define;
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
parcelRequire = function (e, r, t, n) {
  var i,
    o = "function" == typeof parcelRequire && parcelRequire,
    u = "function" == typeof require && require;
  function f(t, n) {
    if (!r[t]) {
      if (!e[t]) {
        var i = "function" == typeof parcelRequire && parcelRequire;
        if (!n && i) return i(t, !0);
        if (o) return o(t, !0);
        if (u && "string" == typeof t) return u(t);
        var c = new Error("Cannot find module '" + t + "'");
        throw c.code = "MODULE_NOT_FOUND", c;
      }
      p.resolve = function (r) {
        return e[t][1][r] || r;
      }, p.cache = {};
      var l = r[t] = new f.Module(t);
      e[t][0].call(l.exports, p, l, l.exports, this);
    }
    return r[t].exports;
    function p(e) {
      return f(p.resolve(e));
    }
  }
  f.isParcelRequire = !0, f.Module = function (e) {
    this.id = e, this.bundle = f, this.exports = {};
  }, f.modules = e, f.cache = r, f.parent = o, f.register = function (r, t) {
    e[r] = [function (e, r) {
      r.exports = t;
    }, {}];
  };
  for (var c = 0; c < t.length; c++) try {
    f(t[c]);
  } catch (e) {
    i || (i = e);
  }
  if (t.length) {
    var l = f(t[t.length - 1]);
    "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = l : "function" == typeof define && define.amd ? define(function () {
      return l;
    }) : n && (this[n] = l);
  }
  if (parcelRequire = f, i) throw i;
  return f;
}({
  "Focm": [function (require, module, exports) {
    function t(r) {
      return (t = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (t) {
        return _typeof(t);
      } : function (t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof(t);
      })(r);
    }
    function r() {
      "use strict";

      r = function r() {
        return e;
      };
      var e = {},
        n = Object.prototype,
        o = n.hasOwnProperty,
        i = Object.defineProperty || function (t, r, e) {
          t[r] = e.value;
        },
        a = "function" == typeof Symbol ? Symbol : {},
        c = a.iterator || "@@iterator",
        u = a.asyncIterator || "@@asyncIterator",
        s = a.toStringTag || "@@toStringTag";
      function l(t, r, e) {
        return Object.defineProperty(t, r, {
          value: e,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }), t[r];
      }
      try {
        l({}, "");
      } catch (P) {
        l = function l(t, r, e) {
          return t[r] = e;
        };
      }
      function f(t, r, e, n) {
        var o = r && r.prototype instanceof y ? r : y,
          a = Object.create(o.prototype),
          c = new j(n || []);
        return i(a, "_invoke", {
          value: E(t, e, c)
        }), a;
      }
      function h(t, r, e) {
        try {
          return {
            type: "normal",
            arg: t.call(r, e)
          };
        } catch (P) {
          return {
            type: "throw",
            arg: P
          };
        }
      }
      e.wrap = f;
      var p = {};
      function y() {}
      function v() {}
      function d() {}
      var g = {};
      l(g, c, function () {
        return this;
      });
      var m = Object.getPrototypeOf,
        w = m && m(m(O([])));
      w && w !== n && o.call(w, c) && (g = w);
      var b = d.prototype = y.prototype = Object.create(g);
      function x(t) {
        ["next", "throw", "return"].forEach(function (r) {
          l(t, r, function (t) {
            return this._invoke(r, t);
          });
        });
      }
      function L(r, e) {
        var n;
        i(this, "_invoke", {
          value: function value(i, a) {
            function c() {
              return new e(function (n, c) {
                !function n(i, a, c, u) {
                  var s = h(r[i], r, a);
                  if ("throw" !== s.type) {
                    var l = s.arg,
                      f = l.value;
                    return f && "object" == t(f) && o.call(f, "__await") ? e.resolve(f.__await).then(function (t) {
                      n("next", t, c, u);
                    }, function (t) {
                      n("throw", t, c, u);
                    }) : e.resolve(f).then(function (t) {
                      l.value = t, c(l);
                    }, function (t) {
                      return n("throw", t, c, u);
                    });
                  }
                  u(s.arg);
                }(i, a, n, c);
              });
            }
            return n = n ? n.then(c, c) : c();
          }
        });
      }
      function E(t, r, e) {
        var n = "suspendedStart";
        return function (o, i) {
          if ("executing" === n) throw new Error("Generator is already running");
          if ("completed" === n) {
            if ("throw" === o) throw i;
            return G();
          }
          for (e.method = o, e.arg = i;;) {
            var a = e.delegate;
            if (a) {
              var c = k(a, e);
              if (c) {
                if (c === p) continue;
                return c;
              }
            }
            if ("next" === e.method) e.sent = e._sent = e.arg;else if ("throw" === e.method) {
              if ("suspendedStart" === n) throw n = "completed", e.arg;
              e.dispatchException(e.arg);
            } else "return" === e.method && e.abrupt("return", e.arg);
            n = "executing";
            var u = h(t, r, e);
            if ("normal" === u.type) {
              if (n = e.done ? "completed" : "suspendedYield", u.arg === p) continue;
              return {
                value: u.arg,
                done: e.done
              };
            }
            "throw" === u.type && (n = "completed", e.method = "throw", e.arg = u.arg);
          }
        };
      }
      function k(t, r) {
        var e = r.method,
          n = t.iterator[e];
        if (void 0 === n) return r.delegate = null, "throw" === e && t.iterator.return && (r.method = "return", r.arg = void 0, k(t, r), "throw" === r.method) || "return" !== e && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + e + "' method")), p;
        var o = h(n, t.iterator, r.arg);
        if ("throw" === o.type) return r.method = "throw", r.arg = o.arg, r.delegate = null, p;
        var i = o.arg;
        return i ? i.done ? (r[t.resultName] = i.value, r.next = t.nextLoc, "return" !== r.method && (r.method = "next", r.arg = void 0), r.delegate = null, p) : i : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, p);
      }
      function S(t) {
        var r = {
          tryLoc: t[0]
        };
        1 in t && (r.catchLoc = t[1]), 2 in t && (r.finallyLoc = t[2], r.afterLoc = t[3]), this.tryEntries.push(r);
      }
      function _(t) {
        var r = t.completion || {};
        r.type = "normal", delete r.arg, t.completion = r;
      }
      function j(t) {
        this.tryEntries = [{
          tryLoc: "root"
        }], t.forEach(S, this), this.reset(!0);
      }
      function O(t) {
        if (t) {
          var r = t[c];
          if (r) return r.call(t);
          if ("function" == typeof t.next) return t;
          if (!isNaN(t.length)) {
            var e = -1,
              n = function r() {
                for (; ++e < t.length;) if (o.call(t, e)) return r.value = t[e], r.done = !1, r;
                return r.value = void 0, r.done = !0, r;
              };
            return n.next = n;
          }
        }
        return {
          next: G
        };
      }
      function G() {
        return {
          value: void 0,
          done: !0
        };
      }
      return v.prototype = d, i(b, "constructor", {
        value: d,
        configurable: !0
      }), i(d, "constructor", {
        value: v,
        configurable: !0
      }), v.displayName = l(d, s, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
        var r = "function" == typeof t && t.constructor;
        return !!r && (r === v || "GeneratorFunction" === (r.displayName || r.name));
      }, e.mark = function (t) {
        return Object.setPrototypeOf ? Object.setPrototypeOf(t, d) : (t.__proto__ = d, l(t, s, "GeneratorFunction")), t.prototype = Object.create(b), t;
      }, e.awrap = function (t) {
        return {
          __await: t
        };
      }, x(L.prototype), l(L.prototype, u, function () {
        return this;
      }), e.AsyncIterator = L, e.async = function (t, r, n, o, i) {
        void 0 === i && (i = Promise);
        var a = new L(f(t, r, n, o), i);
        return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
          return t.done ? t.value : a.next();
        });
      }, x(b), l(b, s, "Generator"), l(b, c, function () {
        return this;
      }), l(b, "toString", function () {
        return "[object Generator]";
      }), e.keys = function (t) {
        var r = Object(t),
          e = [];
        for (var n in r) e.push(n);
        return e.reverse(), function t() {
          for (; e.length;) {
            var n = e.pop();
            if (n in r) return t.value = n, t.done = !1, t;
          }
          return t.done = !0, t;
        };
      }, e.values = O, j.prototype = {
        constructor: j,
        reset: function reset(t) {
          if (this.prev = 0, this.next = 0, this.sent = this._sent = void 0, this.done = !1, this.delegate = null, this.method = "next", this.arg = void 0, this.tryEntries.forEach(_), !t) for (var r in this) "t" === r.charAt(0) && o.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = void 0);
        },
        stop: function stop() {
          this.done = !0;
          var t = this.tryEntries[0].completion;
          if ("throw" === t.type) throw t.arg;
          return this.rval;
        },
        dispatchException: function dispatchException(t) {
          if (this.done) throw t;
          var r = this;
          function e(e, n) {
            return a.type = "throw", a.arg = t, r.next = e, n && (r.method = "next", r.arg = void 0), !!n;
          }
          for (var n = this.tryEntries.length - 1; n >= 0; --n) {
            var i = this.tryEntries[n],
              a = i.completion;
            if ("root" === i.tryLoc) return e("end");
            if (i.tryLoc <= this.prev) {
              var c = o.call(i, "catchLoc"),
                u = o.call(i, "finallyLoc");
              if (c && u) {
                if (this.prev < i.catchLoc) return e(i.catchLoc, !0);
                if (this.prev < i.finallyLoc) return e(i.finallyLoc);
              } else if (c) {
                if (this.prev < i.catchLoc) return e(i.catchLoc, !0);
              } else {
                if (!u) throw new Error("try statement without catch or finally");
                if (this.prev < i.finallyLoc) return e(i.finallyLoc);
              }
            }
          }
        },
        abrupt: function abrupt(t, r) {
          for (var e = this.tryEntries.length - 1; e >= 0; --e) {
            var n = this.tryEntries[e];
            if (n.tryLoc <= this.prev && o.call(n, "finallyLoc") && this.prev < n.finallyLoc) {
              var i = n;
              break;
            }
          }
          i && ("break" === t || "continue" === t) && i.tryLoc <= r && r <= i.finallyLoc && (i = null);
          var a = i ? i.completion : {};
          return a.type = t, a.arg = r, i ? (this.method = "next", this.next = i.finallyLoc, p) : this.complete(a);
        },
        complete: function complete(t, r) {
          if ("throw" === t.type) throw t.arg;
          return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && r && (this.next = r), p;
        },
        finish: function finish(t) {
          for (var r = this.tryEntries.length - 1; r >= 0; --r) {
            var e = this.tryEntries[r];
            if (e.finallyLoc === t) return this.complete(e.completion, e.afterLoc), _(e), p;
          }
        },
        catch: function _catch(t) {
          for (var r = this.tryEntries.length - 1; r >= 0; --r) {
            var e = this.tryEntries[r];
            if (e.tryLoc === t) {
              var n = e.completion;
              if ("throw" === n.type) {
                var o = n.arg;
                _(e);
              }
              return o;
            }
          }
          throw new Error("illegal catch attempt");
        },
        delegateYield: function delegateYield(t, r, e) {
          return this.delegate = {
            iterator: O(t),
            resultName: r,
            nextLoc: e
          }, "next" === this.method && (this.arg = void 0), p;
        }
      }, e;
    }
    function e(t, r, e, n, o, i, a) {
      try {
        var c = t[i](a),
          u = c.value;
      } catch (s) {
        return void e(s);
      }
      c.done ? r(u) : Promise.resolve(u).then(n, o);
    }
    function n(t) {
      return function () {
        var r = this,
          n = arguments;
        return new Promise(function (o, i) {
          var a = t.apply(r, n);
          function c(t) {
            e(a, o, i, c, u, "next", t);
          }
          function u(t) {
            e(a, o, i, c, u, "throw", t);
          }
          c(void 0);
        });
      };
    }
    if (navigator.onLine) {
      var o = function o(t) {
          return t >= "a" && t <= "z" || t >= "A" && t <= "Z";
        },
        i = function () {
          var t = n(r().mark(function t() {
            var e, i, c, s, l, f, h, p, y, v, _d, g, _m;
            return r().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return _m = function m() {
                    return (_m = n(r().mark(function t() {
                      var n, o;
                      return r().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            if (e.length === u) {
                              t.next = 2;
                              break;
                            }
                            return t.abrupt("return");
                          case 2:
                            for (n = e.split(""), o = 0; o < u; o++) n[o] === p[o] ? a[i * u + o].classList.add("correct") : h.includes(n[o]) ? a[i * u + o].classList.add("right") : a[i * u + o].classList.add("wrong");
                            e === h && (c = !0, alert("Congratulations you have won the Game !!!!"), alert("see yaaaa")), e = "", 6 === ++i && (c = !0, alert("sorry  you have lost! please try again !!"));
                          case 8:
                          case "end":
                            return t.stop();
                        }
                      }, t);
                    }))).apply(this, arguments);
                  }, g = function g() {
                    return _m.apply(this, arguments);
                  }, _d = function d() {
                    return (_d = n(r().mark(function t(n) {
                      return r().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            e = e.substring(0, e.length - 1), a[u * i + e.length].innerText = "";
                          case 2:
                          case "end":
                            return t.stop();
                        }
                      }, t);
                    }))).apply(this, arguments);
                  }, v = function v(t) {
                    return _d.apply(this, arguments);
                  }, y = function y(t) {
                    e.length < u ? e += t : (e = e.substring(0, e.length - 1) + t, a[e.length]), a[u * i + e.length - 1].innerText = t;
                  }, e = "", i = 0, c = !1, t.next = 10, fetch("https://words.dev-apis.com/word-of-the-day");
                case 10:
                  return s = t.sent, t.next = 13, s.json();
                case 13:
                  l = t.sent, f = l.word, h = f.toUpperCase(), p = h.split(""), document.addEventListener("keydown", function (t) {
                    var r = t.key;
                    "Enter" === r && 0 == c ? g(r) : "Backspace" === r && 0 == c ? v() : o(r) && 1 == r.length && 0 == c && y(r.toUpperCase());
                  });
                case 18:
                case "end":
                  return t.stop();
              }
            }, t);
          }));
          return function () {
            return t.apply(this, arguments);
          };
        }(),
        a = document.querySelectorAll(".row .sb"),
        c = document.querySelector(".box"),
        u = 5;
      i();
    } else alert("Sorry your internet Connection is down!! please try after some time ");
  }, {}]
}, {}, ["Focm"], null);
},{}]